﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modbus;
using Modbus.Device;
using Modbus.IO;
using Modbus.Data;
using System.Threading;

namespace RoofSimulator
{
	static class Const
	{
		public static ushort Version = 0x0109;

		public static int reg_status_addr = 1;
		//int reg_error_addr = 1;
		public static int reg_command_addr = 2;

		public static int reg_status_no = 2; // В списке регистров
		public static int reg_command_no = 3;
		public static int reg_ext_command_no = 4;
		public static int reg_unlock = 5;
		public static int reg_inputs = 6;
		public static int reg_outputs = 7;

		public static int cmd_open = 1;
		public static int cmd_close = 2;
		public static int cmd_cancel = 0;
		//public static int cmd_power_on = 4;
		//public static int cmd_power_off = 8;
		public static int cmd_cam_light = 0x10;
		public static int cmd_reset = 0x40;
		public static int cmd_park = 0x80;

		public static int cmd_ext_eq_power_on = 1;
		public static int cmd_ext_eq_power_off = 2;
		public static int cmd_ext_power_on = 4;
		public static int cmd_ext_power_off = 8;
		public static int cmd_ext_roof_open = 0x20;
		public static int cmd_ext_roof_close = 0x40;
		public static int cmd_ext_rain_disable = 0x80;

		public static ushort key_unlock_pwr = 0x4C86;

		public static ushort status_opened = 1;
		public static ushort status_closed = 4;
		public static ushort status_opening = 2;
		public static ushort status_closing = 8;
		//public static int status_key_open = 0x10;
		//public static int status_key_close = 0x20;
		public static ushort status_rain = 0x10;
		public static ushort status_park = 0x20;
		public static ushort status_power = 0x40;
		public static ushort status_locked = 0x80;

		public static int status_error_mask = 0xFF80;
	}

	class Program
	{
		static byte modAddr = 1;
		static int tcpPort = 503;
		const double EPS = 1e-6;
		//const double roofMaxSpeed = 1.0/133.0; // 2 минуты 13 секунд
		const double roofMaxSpeed = 1.0 / 10.0;
		const double roofMin = 0.0;
		const double roofMax = 1.0;
		const double roofDelta = 0.01;
		const double parkTime = 10;
		static double roofPosition = 0.5;
		static double roofSpeed = 0.0;
		static long roofStartTime = 0;
		static ushort roofCommand = 0;
		static ushort roofExtCommand = 0;
		static ushort roofUnlock = 0;
		static ushort roofStatus = 0;
		static bool fExit = false;
		static bool fPrintStatus = true;
		static bool fEqPower = true;
		static long EqResetTimer = 0;
		const long EqResetTime = 5;

		public static long currentTimeMillis() {
			return DateTimeOffset.Now.Ticks / 10000; // For profiling purposes
		}
		public static long startTime = currentTimeMillis();
		public static void resetStartTime() {
			startTime = currentTimeMillis();
		}
		public static bool checkTimeout(long startTime, long timeout) {
			long temp = (currentTimeMillis() - startTime);
			if (temp < 0) temp = (long)(ulong)(-1 * temp);//(0xFFFFFFFF-(dword)(-1*temp));
			if (temp >= timeout) return true;
			return false;
		}
		public static bool IsRain { get; set; }
		public static bool IsRainDisabled { get; set; }
		public static bool IsLimitError { get; set; }

		private static int Parking = 0;
		private static long parkStartTime = 0;
		public static bool IsParked { get; set; }
		public static bool Power {
			get { return ((roofStatus & (ushort)Const.status_power) != 0); }
			set { if (value == true) PowerOn(); else PowerOff(); }
		}
		public static bool EqPower {
			get { return fEqPower; }
			set { if (value == true) EqPowerOn(); else EqPowerOff(); }
		}

		public static void Park() {
			Parking = 1;
			ProcessPark();
		}
		public static void UnPark() {
			Parking = -1;
			ProcessPark();
		}
		public static void ProcessPark() {
			if (Parking == 0) {
			} else {
				if (parkStartTime <= 0) parkStartTime = currentTimeMillis();
				else if (checkTimeout(parkStartTime, (long)(parkTime * 1000))) {
					parkStartTime = -1;
					if (Parking > 0) {
						IsParked = true;
					} else if (Parking < 0) {
						IsParked = false;
					}
					Parking = 0;
				}
			}
		}
		static void PowerOff() {
			roofStatus &= (ushort)~Const.status_power;
		}
		static void PowerOn() {
			roofStatus |= (ushort)Const.status_power;
		}
		static void EqPowerOff() {
			fEqPower = false;
			roofExtCommand &= (ushort)~Const.cmd_ext_eq_power_off;
			roofCommand &= (ushort)~Const.cmd_reset;
		}
		static void EqPowerOn() {
			fEqPower = true;
			roofExtCommand &= (ushort)~Const.cmd_ext_eq_power_on;
			roofCommand &= (ushort)~Const.cmd_reset;
		}
		static void EqReset() {
			if (EqPower && EqResetTimer <= 0) {
				EqPowerOff();
				EqResetTimer = currentTimeMillis();
			}
		}
		static void DisplayStatus() {
			string pos_str = " 0 1 2 3 4 5 6 7 8 9 ";
			int pos = (int)((roofPosition * (pos_str.Length-1)) + 0.4999);
			if (pos < 0) pos = 0;
			if (pos >= pos_str.Length) pos = pos_str.Length - 1;
			pos_str = pos_str.Remove(pos, 1).Insert(pos, "*");

			Console.SetCursorPosition(0, 0);
			Console.WriteLine("Cmd:{0:X4} Ext:{1:X4} Lck:{2:X4} Status:{3:X4}       ", roofCommand, roofExtCommand, roofUnlock, roofStatus);
			Console.WriteLine("Parked:{0}({1}) Rain:{2}(Dis:{3}) Limit:{4}       ", IsParked, Parking, IsRain, IsRainDisabled, IsLimitError);
			Console.WriteLine("Power:{0} EqPower:{1}       ", Power, EqPower);
			Console.WriteLine("");
			Console.WriteLine("Position: {0:F2} [{1}]   ", roofPosition, pos_str);
			Console.WriteLine("");
			if (IsParked) Console.WriteLine("PARKED (P: Unpark) "); else Console.WriteLine("Not parked (P: Park) ");
			if (IsRain) Console.Write("RAIN (R: Off) "); else Console.Write("Dry (R: Rain) ");
			if (IsRainDisabled) Console.WriteLine(" DISABLED"); else Console.WriteLine("");
			if (IsLimitError) Console.WriteLine("LIMIT REACHED (L: Off)         "); else Console.WriteLine("Limit not reached (L: Set error) ");
			//Console.WriteLine("");
			Console.WriteLine("<-, -> - move roof, ESC - Exit");
		}

		private static bool ProcessKeys(ConsoleKeyInfo keyInfo) {
			if (keyInfo == null) return false;
			switch (keyInfo.Key) {
				case ConsoleKey.Escape:
					fExit = true;
					return true;
				break;
				case ConsoleKey.R:
					IsRain = !IsRain;
					fPrintStatus = true;
				break;
				case ConsoleKey.L:
					IsLimitError = !IsLimitError;
					fPrintStatus = true;
				break;
				case ConsoleKey.P:
					IsParked = !IsParked;
					fPrintStatus = true;
				break;
				case ConsoleKey.RightArrow:
					roofPosition += 0.05;
					if (roofPosition > 1.0) roofPosition = 1.0;
					fPrintStatus = true;
				break;
				case ConsoleKey.LeftArrow:
					roofPosition -= 0.05;
					if (roofPosition < 0.0) roofPosition = 0.0;
					fPrintStatus = true;
				break;
			}
			return true;
		}

		private static Modbus.Device.ModbusSlave slave;
		private static DataStore dataStore;
		static void Main(string[] args) {
			Console.WriteLine("Modbus Roof simulator started");
			slave = ModbusTcpSlave.CreateTcp(modAddr, new System.Net.Sockets.TcpListener(tcpPort));
			slave.DataStore = dataStore = DataStoreFactory.CreateDefaultDataStore(0, 0, 16, 16);
			slave.ModbusSlaveRequestReceived += new EventHandler<ModbusSlaveRequestEventArgs>((object obj, ModbusSlaveRequestEventArgs arg) => {
				//Console.WriteLine("OnRequest: Obj={0}, Args={1}", obj, arg);
				fPrintStatus = true;
			});
			dataStore.HoldingRegisters[1] = Const.Version;
			dataStore.InputRegisters[1] = Const.Version;
			while (!fExit) {
				try {
					slave.Listen();
					Console.WriteLine("Listening at port {0}", tcpPort);
					while (!fExit) {
						if (Console.KeyAvailable) {
							ConsoleKeyInfo keyInfo;
							keyInfo = Console.ReadKey();
							ProcessKeys(keyInfo);
						}
						lock (dataStore.SyncRoot) {
							roofCommand = dataStore.HoldingRegisters[Const.reg_command_no];
							roofExtCommand = dataStore.HoldingRegisters[Const.reg_ext_command_no];
							roofUnlock = dataStore.HoldingRegisters[Const.reg_unlock];
							ProcessCommand(roofCommand);
							dataStore.InputRegisters[Const.reg_status_no] = roofStatus;
							dataStore.InputRegisters[Const.reg_command_no] = roofCommand;
							dataStore.InputRegisters[Const.reg_ext_command_no] = roofExtCommand;
							dataStore.InputRegisters[Const.reg_unlock] = roofUnlock;
							dataStore.HoldingRegisters[Const.reg_status_no] = roofStatus;
							dataStore.HoldingRegisters[Const.reg_command_no] = roofCommand;
							dataStore.HoldingRegisters[Const.reg_ext_command_no] = roofExtCommand;
							dataStore.HoldingRegisters[Const.reg_unlock] = roofUnlock;
						}
						if (fPrintStatus) {
							DisplayStatus();
							fPrintStatus = false;
						}
						Thread.Sleep(10);
					}
				} catch (Exception e) {
					Console.WriteLine(e.Message);
					Console.WriteLine(e.StackTrace);
				}
			}
		}
		public static bool ProcessCommand(ushort cmd) {
			if ((cmd & Const.cmd_open) != 0) roofSpeed = roofMaxSpeed;
			if ((cmd & Const.cmd_close) != 0) {
				if (IsParked) roofSpeed = -roofMaxSpeed;
				else Park();
			}
			if ((cmd & (Const.cmd_open | Const.cmd_close)) == 0) roofSpeed = 0;
			if (IsRain) {
				roofStatus |= Const.status_rain;
				if (IsParked) roofSpeed = -roofMaxSpeed;
				else Park();
			}
			ProcessPark();
			if (IsLimitError) {
				if (roofUnlock != Const.key_unlock_pwr) {
					PowerOff();
					roofUnlock = 0xFFFF;
				} else {//if (roofUnlock != 0xFFFF) {
					//roofUnlock = 0xFFFF;
					if ((roofExtCommand & Const.cmd_ext_power_on) != 0) PowerOn();
				}
			} else {
				roofUnlock = 0;
				if ((roofExtCommand & Const.cmd_ext_power_on) != 0) PowerOn();
			}
			if ((roofExtCommand & Const.cmd_ext_power_off) != 0) PowerOff();
			if ((roofExtCommand & Const.cmd_ext_eq_power_on) != 0) EqPowerOn();
			if ((roofExtCommand & Const.cmd_ext_eq_power_off) != 0) EqPowerOff();
			if ((roofCommand & Const.cmd_reset) != 0) EqReset();
			IsRainDisabled = ((roofExtCommand & Const.cmd_ext_rain_disable) != 0);

			if (Math.Abs(roofSpeed) > EPS) { // Движение есть!
				if (roofStartTime <= 0) roofStartTime = currentTimeMillis();
				else if (checkTimeout(roofStartTime, 100)) {
					roofStartTime = currentTimeMillis();
					if (roofSpeed < 0) {
						if (roofPosition > roofMin) roofPosition += roofSpeed * 0.1;
						else {
							roofSpeed = 0;
						}
					} else {
						if (roofPosition < roofMax) roofPosition += roofSpeed * 0.1;
						else {
							roofSpeed = 0;
						}
					}
				}
			}
			if (EqResetTimer > 0) {
				if (checkTimeout(EqResetTimer, EqResetTime*1000)) {
					EqResetTimer = -1;
					EqPowerOn();
					roofCommand &= (ushort)~Const.cmd_reset;
				}
			}
			roofStatus &= (ushort)~(Const.status_opening | Const.status_closing | Const.status_closed | Const.status_opened);
			if (roofSpeed > 0) roofStatus |= Const.status_opening;
			if (roofSpeed < 0) roofStatus |= Const.status_closing;
			if (roofPosition < roofMin + roofDelta) {
				roofStatus |= Const.status_closed;
				PowerOff();
			}
			if (roofPosition > roofMax - roofDelta) {
				roofStatus |= Const.status_opened;
				PowerOn();
				if (IsParked) UnPark();
			}
			if (IsParked) roofStatus |= Const.status_park; else roofStatus &= (ushort)~Const.status_park;
			if (IsRain) roofStatus |= Const.status_rain; else roofStatus &= (ushort)~Const.status_rain;
			if (Power) roofStatus |= Const.status_power; else roofStatus &= (ushort)~Const.status_power;
			return true;
		}
	}
}
